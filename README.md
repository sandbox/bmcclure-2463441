This is a very simplistic alternative to Views Dependent Filters which is managed entirely in code without modifying the view itself.

This module simply invokes a new hook, hook_views_filter_dependencies. Implement the hook in your own module like this:

<?php
function YOUR_MODULE_views_filter_dependencies() {
  $filter_dependencies['view_name']['defaults'] = array(
    'field_something' => 'Default Value',
  );

  $filter_dependencies['view_name']['fields'] = array(
    'field_dependent_field' => array(
      'default_value' => 'All',
      'dependencies' => array(
        'field_something' => 'Some Value',
        'field_something_else' => array('Some Value', 'Some Other Value'),
      ),
    ),
  );

  return $filter_dependencies;
}
?>

In the above example, this set of dependencies apply to the view with the machine name view_name.

The 'defaults' key provides a value to assign to any fields that are missing from the form data. You may not even need to use the defaults, but in some cases it can be helpful.

The 'fields' key takes an array of fields and field settings. The field settings define a "default_value" key which specifies what value to set for the field when it is hidden. It then defines a "dependencies" key containing an array of fields and values that are required for the field to show.

In the above example, The filter for "field_dependent_field" will show only if the value of the "field_something" filter is "Some Value" and the value of the "field_something_else" filter is either "Some Value" or "Some Other Value." Because of the defaults specified, if "field_something" has no value in $form_state, then it is assumed to have the value "Default Value."

You can also implement hook_views_filter_dependencies_alter(&$filter_dependencies) to modify existing filter dependencies. Each view can only be defined once, so if you need to define dependencies for a view from multiple different modules, then you may wish to use the alter function so that you can combine the filters within one view key.

Currently this is the extent of the functionality. The comparison operator for values is always !== (So if the filter value !== one of the values you specify, the field is hidden).
