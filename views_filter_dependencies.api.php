<?php
/**
 * Created by PhpStorm.
 * User: BMcClure
 * Date: 3/31/2015
 * Time: 10:09 PM
 */

/**
 * Return an array of view IDs which each contain an array of fields and their
 * dependencies, and optionally default values for any fields that are missing.
 */
function hook_views_filter_dependencies() {
  $filter_dependencies['view_name']['defaults'] = array(
    'field_something' => 'Default Value',
  );

  $filter_dependencies['view_name']['fields'] = array(
    'field_dependent_field' => array(
      'default_value' => 'All',
      'dependencies' => array(
        'field_something' => 'Some Value',
        'field_something_else' => array('Some Value', 'Some Other Value'),
      ),
    ),
  );

  return $filter_dependencies;
}
